<div align="center">
![Desktop](Screenshots/Screenshot_2020-07-29-39_1920x1080.png)

```
             ______      _  ______ _____ _              _                          
             |  _  \    | | |  ___|_   _| |            | |                         
             | | | |___ | |_| |_    | | | | ___  ___   | |__  _   _                
             | | | / _ \| __|  _|   | | | |/ _ \/ __|  | '_ \| | | |               
             | |/ / (_) | |_| |    _| |_| |  __/\__ \  | |_) | |_| |               
             |___/ \___/ \__\_|    \___/|_|\___||___/  |_.__/ \__, |               
                                                               __/ |               
                                                              |___/                
______           _          ___  ___      _   _          ___________  _____  _____ 
|  _  \         | |         |  \/  |     | | | |        |___  / __  \|____ |/ __  \
| | | |__ _ _ __| | ________| .  . | __ _| |_| |_ ___ _ __ / /`' / /'    / /`' / /'
| | | / _` | '__| |/ /______| |\/| |/ _` | __| __/ _ \ '__/ /   / /      \ \  / /  
| |/ / (_| | |  |   <       | |  | | (_| | |_| ||  __/ |./ /  ./ /___.___/ /./ /___
|___/ \__,_|_|  |_|\_\      \_|  |_/\__,_|\__|\__\___|_|\_/   \_____/\____/ \_____/
                                                                                   
                                                                                   
```

![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)
![forthebadge](https://forthebadge.com/images/badges/made-with-crayons.svg)
</div>

#  Welcome to my dotfiles! :space_invader:  

I wrote my bspwm configs to make my workflow smoother.

I'm including some screenshots on this repo:wink:  
# 
# Keybindings reference

This is a rundown of all keybindings.
They are just for reference

You can access this list by pressing Super + Alt + I

## General

| Binding                              | Funcionality                                    |
| -------------------------------------| ------------------------------------------------|
| Super                                | Open text based application launcher            |
| Super + Q                            | Close Application                               |
| Super + T                            | Tiled Mode                                      |
| Super + P                            | Pseudo-Tiled Mode                               |
| Super + Space                        | Toggle beetwen floating & tiled                 |
| Super + F                            | Monocle Mode/Fullscreen                         |         
| Super + Number                       | Switch to Workspace Number                      |      
| Super + Tab                          | Switch to last opened workspace                 |
| Super + Shift + Number               | Move window to Workspace number                 |     
| Super + Return                       | Open terminal                                   |
| Super + B                            | Launch Brave                                    |
| Super + Shift + Return               | Floating terminal                               | 
| Super + Shift + Arrow Keys           | Move windows around                             |
| Super + Ctrl + Number                | Toggle on additional tag                        |
| Super + Alt + Number                 | Move window to tag number                       |
| Super + Arrow Keys                   | Switch focus                                    |
| Super + W                            | Toggle Window switcher                          |        
| Ctrl + Alt + L                       | Lock screen                                     |
| Super + X                            | Open shutdown menu                              |
| Ctrl + Alt + R                       | Restart Bspwm                                   |
| Ctrl + Alt + Q                       | Close Bspwm                                     |
| Super + Escape                       | Reload Keybindings                              |
| Ctrl + Alt + Escape                  | Kill a program                                  |
| Super + H,V,C                        | Split horizontal, vertical or cancel            |
| Super + Ctrl + M,X,Y,Z               | Set the node flags(marked,locked,sticky,private)|
| Super + Shift + Left,Down,Up,Right   | Send the window to another edge of the screen   |
| Ctrl + Alt + Left,Right              | Switch workspace                                |
| Super + Scroll                       | Change window gap                               |

       
       
## Terminal       
       
| Binding                             | Funcionality     |
| ------------------------------------| ---------------- |
| Ctrl + Shift + C                    | Copy             |
| Ctrl + Shift + V                    | Paste            |
| Ctrl + +/-                          | Adjust font size |

## Desktop
These are only active when there is no window on the current tag

| Binding    | Funcionality         |
| ---------- | ---------------------|
| Super + X  | Open PowerMenu       |
| Super + r  | Open AsrootMenu      |
